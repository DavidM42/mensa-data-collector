#!/usr/bin/env python
# -*- coding: utf-8 -*-

#MEANT TO BE in python 2 for now

from bs4 import BeautifulSoup  
import urllib as urllib2
import time
import pprint
from pymongo import MongoClient
from datetime import datetime
import ssl

alreadyErrored = False

#TODO port to python3 -> change ConfigParser to configparser and urrlib request changen https://docs.python.org/3/howto/urllib2.html
from ConfigParser import ConfigParser, NoOptionError, NoSectionError

config = ConfigParser()
#config.read('config.ini')
config.read('/home/pi/mensa-data-collector/config.ini')

username = urllib2.quote_plus(config.get("general", 'username'))
password = urllib2.quote_plus(config.get("general", 'password'))

# python 3 version for porting
# import urllib.parse
# username = urllib.parse.quote_plus(config.get("general", 'username'))
# password = urllib.parse.quote_plus(config.get("general", 'password'))

client = MongoClient(config.get("general", 'url'),
                    port=  int(config.get("general", 'port')),
                    username=username,
                    password=password,
                    authSource= config.get("general", 'database'),
                    authMechanism='SCRAM-SHA-1',    
                    ssl=True,ssl_cert_reqs=ssl.CERT_NONE)
db = client[config.get("general", 'database')]

from slackclient import SlackClient

slack_token = config.get("general", 'slack_token')
sc = SlackClient(slack_token)
# print(sc.api_call("groups.list"))
data_mining_channel = config.get("general", 'slack_channel')


def parseDate(date_string):
    date_string = date_string.split(" ")[1]
    day_date = datetime.strptime(date_string, '%d.%m.').replace(year=datetime.now().year)
    # TODO doesnt work if listed days is next year so fix this year change bug
        # date_string = "02.01."
        # print((day_date - datetime.now()).total_seconds())
        # if (day_date - datetime.now()).total_seconds() < 0.0:
        #     newYear = datetime.now().year+1
        #     day_date.replace(year=2019)
        #     print(day_date)
    return day_date

def returnAdditives(day_dish):
    additiveArr = []
    # very specific and step for step parsing but maybe change problematic
    # additives_temp = day_dish.find("div", {"class": "additnr"}).find("div", {"class": "toggler"}).find("ul").find_all("li")
    additives_temp = day_dish.find("div", {"class": "additnr"}).find("ul").find_all("li")
    for additive in additives_temp:
        additiveArr.append(additive.text)
    return additiveArr


def dishes_to_db(soup, menue_collection):
    global alreadyErrored
    # get both weeks
    weeks = soup.body.find_all("div", {"class": "week"})
    #TODO get notices (desert prices and so on) and save info with timestamp of getting

    dishes = []

    for week in weeks: 
        week_kw = int(week["data-kw"].replace("kw", ""))
        days = week.find_all("div", {"class": "day"})
        for day in days:
            day_date= parseDate(day["data-day"])
            day_dishes = day.find_all("article", {"class": "menu"})
            for day_dish in day_dishes:
                #initialise dish with attributes set to false so they exist in every document as default false if not set to true later
                dish = {"Alkohol": False, "Fisch": False,"Fleischlos": False,"Gefluegel": False,"Kalb": False,
                "Lamm": False,"Rind": False,"Schwein": False,"Vegan": False,"Vorderschinken": False, "Wild": False}
                
                dish["kw"] = week_kw
                dish["date"] = day_date

                dish["name"] = day_dish.find("div", {"class": "title"}).text
                dish["additives"] = returnAdditives(day_dish)

                #init prices to be none
                dish["price_stud"] = None
                dish["price_bed"] = None
                dish["price_guest"] = None
                try:
                    #try to get price from html soup standard
                    dish["price_stud"] = day_dish.find("div", {"class": "price"})["data-default"]
                    dish["price_bed"] = day_dish.find("div", {"class": "price"})["data-bed"]
                    dish["price_guest"] = day_dish.find("div", {"class": "price"})["data-guest"]
                except TypeError as e:
                    #no price found for this dish 
                    #probably cause meal is in past so no prices in web page
                    #try to get price from previous entry of this into db
                    old_dish = menue_collection.find_one({'name': dish['name'], "date": dish["date"]})
                    try:
                        dish["price_stud"] = old_dish["price_stud"]
                        dish["price_bed"] = old_dish["price_bed"]
                        dish["price_guest"] = old_dish["price_guest"]                
                    except (KeyError,TypeError) as e:
                        #could really not find price not in html and also there is no older db entry
                        # print(e)
                        pass

                dish["type"] = day_dish.find("div", {"class": "icon"})["title"]

                attributes = day_dish.find("div", {"class": "icon"}).find_all("div", {"class": "theicon"})
                for attribute in attributes:
                    dish[attribute["title"]] = True
                
                # dont know what todo but maybe unique identifier
                dish["did"] = day_dish.find("div", {"class": "icon"})["data-did"]

                dishes.append(dish)


    for dish in dishes:
        #TODO fix this error handling cause doenst work yet but I have no patience left
        try:
            dish_id = menue_collection.update({'name': dish['name'], "date": dish["date"]}, dish, upsert=True)
        except Exception as e:
            if not alreadyErrored:
                result = sc.api_call(
                    "chat.postMessage",
                    channel=data_mining_channel,
                    text=":hamburger: Error: " + str(e.message) + " while saving mensa dishes to DB",
                    thread_ts="1476746830.000003"
                )     
                print(e)
                alreadyErrored = True

        # TODO maybe this edits if something changed as detail of dish but not if dish gets removed because checks for name as id not if name still exists in html
        # TODO so make method to remove all dishes of days still present in html but dish not listed in html anymore but db



def main():
    global alreadyErrored
    all_mensen = []
    for each_section in config.sections():
        if each_section != "general":
            menue_collection = db[each_section]
            url = config.get(each_section, 'url')

            # for now prefer stacktrace of error over slack message with error

            #Get html page of menue
            # try:
            html_page = html_page = urllib2.urlopen(url).read()

            soup = BeautifulSoup(html_page, 'html.parser')                                

            # print("Will now collect dishes and save to db for mensa: " + each_section)
            all_mensen.append(each_section)
            dishes_to_db(soup, menue_collection)

            # except Exception as e:
            #     print(e.message)
            #     if not alreadyErrored:
            #         result = sc.api_call(
            #             "chat.postMessage",
            #             channel=data_mining_channel,
            #             text=":hamburger: Error: " + str(e.message) + " while fetching mensa dishes from website",
            #             thread_ts="1476746830.000003"
            #         )     
            #         print(e)
            #         alreadyErrored = True
    print("Saved dishes for mensen: " + str(all_mensen).strip('[]').replace("'", ""))
    result = sc.api_call(
        "chat.postMessage",
        channel=data_mining_channel,
        text=":hamburger: Saved dishes for mensen: " + str(all_mensen).strip('[]').replace("'", ""),
        thread_ts="1476746830.000003"
    )

if __name__ == "__main__":
    main()
