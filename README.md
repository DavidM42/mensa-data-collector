# Mensa-Data-Collector

## How to use
* Copy `config.ini.example` to `config.ini`
* Edit the example general section with your own ip and port
* _Optional: change sections for other mensas or edit urls of existing entries_
* _Optional: Create a virtual environmnent if you want to create a more isolated dependancy installation_
* Install the dependancies via `pip install -r requirements.txt`
* Run the script `python parser.py`
* Wait for it to iterate over all entered mensas and save all dishes with all details to your mongoDB